public class FindPrimes {

    public static void main(String[] args){
        int max = Integer.parseInt(args[0]);

            //prints the numbers less than max
            // for each number less than max
            for(int number = 2; number < max; number++) {
                //let divisor =2;
                int divisor = 2;
                boolean isPrime = true;
                //while divisor less than number
                while (divisor < number) {
                    // if the number is divisible by divisor
                    if (number % divisor == 0)
                        isPrime = false;  //isprime = false
                    //increment divisor
                    divisor++;
                }

                //if the number is prime
                if (isPrime)
                    System.out.print(number + " ");
                //print the number
            }
    }
}
