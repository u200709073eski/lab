package stack;

import java.util.ArrayList;
import java.util.List;

public class StackArrayListImpl implements Stack {

    private List<Object> stack = new ArrayList<>();

    public void push(Object item) {
        stack.add(item);
    }

    public Object pop() {
        if (stack.size()> 0)
        return stack.remove(stack.size()-1);
        return null;
    }

    public boolean empty() {
        return stack.size() ==0;
    }
}
