package stack;

public class StackImpl implements Stack {

    private StackItem top;

    public void push(Object item) {
        StackItem stackItem = new StackItem(item);
        stackItem.setNext(top);
        top = stackItem;

    }

    public Object pop() {
        if (top != null) {
            Object item = top.getItem();
            top = top.getNext();
            return item;
        }
        return null;
    }
    public boolean empty() {
        return top == null;
    }
}
