public class Rectangle {
    private Point topleft;
    private int width;
    private int height;


    public Rectangle(Point topleft, int width, int height) {
        this.topleft = topleft;
        this.width = width;
        this.height = height;
    }

    public int area() {
        return width * height;
    }

    public int perimeter() {
        return 2 * (width * height);
    }

    public Point[] corners() {
        Point[] corners = new Point[4];

        corners[0] = topleft;
        corners[1] = new Point(topleft.getxCoord()+width,topleft.getyCoord());
        corners[2] = new Point(topleft.getxCoord(),topleft.getyCoord()-height);
        corners[3] = new Point(topleft.getxCoord()+width,topleft.getyCoord()-height);
        return corners;

    }
}